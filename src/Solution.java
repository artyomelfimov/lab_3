import abstract_factory.Commander;
import abstract_factory.Soldier;
import goblin.GoblinFactory;
import orc.OrcFactory;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("""
                Введите номер для выбора фабрики:
                1. Фабрика орков
                2. Фабрика гоблинов
                """);

        Army army = new Army();
        int choice = scanner.nextInt();
        scanner.close();

        switch (choice) {
            case 1 -> army.setWarriorFactory(new OrcFactory());
            case 2 -> army.setWarriorFactory(new GoblinFactory());
        }

        army.addCommander();
        army.addSoldier();

        Commander commander = army.getCommander();
        commander.order();
        Soldier soldier = army.getSoldierByIndex(0);
        soldier.fight();
    }
}
