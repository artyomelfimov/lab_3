package goblin;

import abstract_factory.Commander;
import abstract_factory.Soldier;
import abstract_factory.WarriorFactory;

public class GoblinFactory implements WarriorFactory {
    @Override
    public Commander createCommander() {
        return new GoblinCommander();
    }

    @Override
    public Soldier createSoldier() {
        return new GoblinSoldier();
    }
}
