package goblin;

import abstract_factory.Soldier;

public class GoblinSoldier implements Soldier {
    @Override
    public void fight() {
        System.out.println("Прикройте меня");
    }
}
