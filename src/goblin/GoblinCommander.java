package goblin;

import abstract_factory.Commander;

public class GoblinCommander implements Commander {
    @Override
    public void order() {
        System.out.println("Атаковать левым флангом");
    }
}
