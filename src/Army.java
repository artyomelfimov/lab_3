import abstract_factory.Commander;
import abstract_factory.Soldier;
import abstract_factory.WarriorFactory;

import java.util.ArrayList;
import java.util.List;

public class Army {
    private WarriorFactory warriorFactory;
    private Commander commander;
    private List<Soldier> soldiers = new ArrayList<>();

    public void addCommander() {
        if (!isFactoryCreated()) return;
        this.commander = warriorFactory.createCommander();
    }

    public Commander getCommander() {
        return commander;
    }

    public Soldier getSoldierByIndex(int index) {
        return soldiers.get(index);
    }

    public void addSoldier() {
        if (!isFactoryCreated()) return;
        Soldier soldier = warriorFactory.createSoldier();
        soldiers.add(soldier);
    }

    public WarriorFactory getWarriorFactory() {
        return warriorFactory;
    }

    public void setWarriorFactory(WarriorFactory warriorFactory) {
        this.warriorFactory = warriorFactory;
    }

    private boolean isFactoryCreated() {
        if (warriorFactory == null) {
            System.out.println("Фабрика не выбрана");
            return false;
        }
        return true;
    }
}
