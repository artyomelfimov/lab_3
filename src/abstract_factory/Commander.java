package abstract_factory;

public interface Commander {
    void order();
}
