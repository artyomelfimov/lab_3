package abstract_factory;

public interface Soldier {
    void fight();
}
