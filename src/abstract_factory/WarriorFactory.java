package abstract_factory;

public interface WarriorFactory {
    Commander createCommander();
    Soldier createSoldier();
}
