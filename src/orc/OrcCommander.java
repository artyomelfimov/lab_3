package orc;

import abstract_factory.Commander;

public class OrcCommander implements Commander {
    @Override
    public void order() {
        System.out.println("Атаковать правым флангом!");
    }
}
