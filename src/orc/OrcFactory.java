package orc;

import abstract_factory.Commander;
import abstract_factory.Soldier;
import abstract_factory.WarriorFactory;

public class OrcFactory implements WarriorFactory {
    @Override
    public Commander createCommander() {
        return new OrcCommander();
    }

    @Override
    public Soldier createSoldier() {
        return new OrcSoldier();
    }
}
