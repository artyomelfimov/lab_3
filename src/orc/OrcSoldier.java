package orc;

import abstract_factory.Soldier;

public class OrcSoldier implements Soldier {
    @Override
    public void fight() {
        System.out.println("Нужна помощь!");
    }
}
